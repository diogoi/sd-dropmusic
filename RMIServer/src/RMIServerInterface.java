import org.json.simple.JSONObject;
import java.rmi.Remote;
import java.rmi.RemoteException;

public interface RMIServerInterface extends Remote {
    String testConnection() throws RemoteException;
    String signUp(String username, String password) throws RemoteException;
    JSONObject signIn(String username, String password) throws RemoteException;
    String createArtist(String name) throws RemoteException;
    String createAlbum(String name, String artist, String info, double user_id) throws RemoteException;
    String createMusic(String name, String artist, String album) throws RemoteException;
    String deleteMusic(String mucisc_name, String album_name, String artist_name) throws RemoteException;
    String deleteAlbum(String album_name, String artist_name) throws RemoteException;
    String deleteArtist(String artist_name) throws RemoteException;
    String writeAlbumReview(String artist, String album, String rate, String review, double user_id) throws RemoteException;
    String editArtist(String name, String newName) throws RemoteException;
    String editAlbum(String name, String newName, String artist, String new_info, double user_id) throws RemoteException;
    String editMusic(String name, String newName, String artist, String album) throws RemoteException;
    String searchArtist(String name ) throws RemoteException;
    String searchAlbum(String name, String artist) throws RemoteException;
    String searchMusic(String name, String artist, String album) throws RemoteException;
    String checkMessages(double user_id) throws RemoteException;
    void logout(double user_id) throws RemoteException;
    JSONObject downloadMusic(String artist_name, String album_name, String music_name) throws RemoteException;
    JSONObject globalSearch(String name) throws RemoteException;
    String getAlbumById(String albumId) throws RemoteException;
    String getArtistById(String albumId) throws RemoteException;
    JSONObject uploadMusic(String artist_name, String album_name, String music_name, String filepath, int port) throws RemoteException;
    void ping() throws RemoteException;
    String givePowers(String username) throws RemoteException;
}