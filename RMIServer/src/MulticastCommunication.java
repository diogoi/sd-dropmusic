import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;


import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.MulticastSocket;

public class MulticastCommunication {
    private Long id = new Long(0);
    private int MultiCastSendPort = 3333;
    private int MultiCastReceivePort = 3334;
    private String MultiCastAddress = "224.0.224.0";
    private final Object lock = new Object();

    public JSONObject exchange(JSONObject msg){
        JSONObject msgObj = null;
        try {
            Long message_id = send(msg);
            msgObj = receive(message_id);
        } catch (IOException | ParseException e) {
            System.out.println(e);
            e.printStackTrace();
        }
        return msgObj;
    }

    /**
     * Method to create a socket, identify the packet and send a JSON Object
     */
    private Long send(JSONObject msg) throws IOException {
        Long message_id = new Long(0);
        synchronized (this.lock) {
            msg.put("message_id", id);
            message_id = id;
            this.id += 1;
        }
        byte[] buffer = msg.toString().getBytes();
        MulticastSocket socket = new MulticastSocket(this.MultiCastSendPort);
        InetAddress group = InetAddress.getByName(this.MultiCastAddress);
        DatagramPacket packet = new DatagramPacket(buffer, buffer.length, group, this.MultiCastSendPort);
        socket.send(packet);
        return message_id;
    }

    /**
     * Method to create a socket and get information.
     */
    private JSONObject receive(Long message_id) throws IOException, ParseException {
        MulticastSocket socket = new MulticastSocket(this.MultiCastReceivePort);
        InetAddress group = InetAddress.getByName(this.MultiCastAddress);
        socket.joinGroup(group);
        byte[] buffer = new byte[1024];
        Long m_id = new Long(-1);
        JSONObject msgObject = null;
        JSONParser parser = new JSONParser();
        while(!m_id.equals(message_id)) {
            DatagramPacket packet = new DatagramPacket(buffer, buffer.length);
            socket.receive(packet);
            Object obj = parser.parse(new String(packet.getData(), 0, packet.getLength()));
            msgObject = (JSONObject) obj;
            m_id = (Long)msgObject.get("message_id");
        }
        System.out.println("RESPONSE");
        System.out.println(msgObject);
        System.out.println(msgObject.get("address"));
        return msgObject;
    }
}