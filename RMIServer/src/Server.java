import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.rmi.AlreadyBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.ServerNotActiveException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

public class Server extends UnicastRemoteObject implements RMIServerInterface {
    private int RMIPort = 3999;
    private Registry r;
    private boolean isLoggedIn = false;
    private MulticastCommunication communicate = new MulticastCommunication();
    private Notification notification = new Notification();

    public Server() throws RemoteException, AlreadyBoundException {
        super();
    }

    public String editAlbum(String name, String newName, String artist, String new_info, double user_id) {
        JSONObject msg = new JSONObject();
        msg.put("type", "edit_album");
        msg.put("album_name", name);
        msg.put("new_album_name", newName);
        msg.put("artist_name", artist);
        msg.put("info", new_info);
        msg.put("user_id", user_id);
        JSONObject response = communicate.exchange(msg);
        notification.sendNotification(response);
        return (String) response.get("message");
    }

    public String checkMessages(double user_id) {
        return notification.hasTasks(user_id);
    }

    public String editMusic(String name, String newName, String artist, String album) {
        JSONObject msg = new JSONObject();
        msg.put("type", "edit_music");
        msg.put("music_name", name);
        msg.put("artist_name", artist);
        msg.put("album_name", album);
        JSONObject response = communicate.exchange(msg);
        return (String) response.get("message");
    }

    public String editArtist(String name, String newName) {
        JSONObject msg = new JSONObject();
        msg.put("type", "edit_artist");
        msg.put("artist_name", name);
        msg.put("new_artist_name", newName);
        JSONObject response = communicate.exchange(msg);
        return (String) response.get("message");
    }

    public String deleteArtist(String artist_name) {
        JSONObject msg = new JSONObject();
        msg.put("type", "delete_artist");
        msg.put("artist_name", artist_name);
        JSONObject response = communicate.exchange(msg);
        return (String) response.get("message");
    }

    public String deleteAlbum(String album_name, String artist_name) {
        JSONObject msg = new JSONObject();
        msg.put("type", "delete_album");
        msg.put("album_name", album_name);
        msg.put("artist_name", artist_name);
        JSONObject response = communicate.exchange(msg);
        return (String) response.get("message");
    }

    public String deleteMusic(String music_name, String album_name, String artist_name) {
        JSONObject msg = new JSONObject();
        msg.put("type", "delete_music");
        msg.put("music_name", music_name);
        msg.put("album_name", album_name);
        msg.put("artist_name", artist_name);
        JSONObject response = communicate.exchange(msg);
        return (String) response.get("message");
    }

    public String testConnection() {
        System.out.println("RMIClient connected");
        return "Connected to RMIServer";
    }

    public String signUp(String username, String password) {
        JSONObject msg = new JSONObject();
        msg.put("type", "register");
        msg.put("username", username);
        msg.put("password", password);
        JSONObject response = communicate.exchange(msg);
        return (String) response.get("message");
    }

    public JSONObject signIn(String username, String password) {
        JSONObject msg = new JSONObject();
        msg.put("type", "login");
        msg.put("username", username);
        msg.put("password", password);
        JSONObject response = communicate.exchange(msg);
        if ((boolean) response.get("success")) {
            this.notification.addActiveUser((double) response.get("user_id"));
        }
        return response;
    }

    public String createArtist(String name) {
        JSONObject msg = new JSONObject();
        msg.put("type", "create_artist");
        msg.put("name", name);
        JSONObject response = communicate.exchange(msg);
        return (String) response.get("message");
    }

    public String createAlbum(String name, String artist, String info, double user_id) {
        JSONObject msg = new JSONObject();
        msg.put("type", "create_album");
        msg.put("name", name);
        msg.put("artist", artist);
        msg.put("info", info);
        msg.put("user_id", user_id);
        JSONObject response = communicate.exchange(msg);
        return (String) response.get("message");
    }

    public String createMusic(String name, String artist, String album) {
        JSONObject msg = new JSONObject();
        msg.put("type", "create_music");
        msg.put("name", name);
        msg.put("artist", artist);
        msg.put("album", album);
        JSONObject response = communicate.exchange(msg);
        return (String) response.get("message");
    }

    public String writeAlbumReview(String artist, String album, String rate, String review, double user_id) {
        JSONObject msg = new JSONObject();
        msg.put("type", "write_album_review");
        msg.put("album_name", album);
        msg.put("artist_name", artist);
        msg.put("rate", rate);
        msg.put("review", review);
        msg.put("user_id", user_id);
        JSONObject response = communicate.exchange(msg);
        System.out.println(response.toString());
        return (String) response.get("message");
    }

    public String searchArtist(String artist_name) {
        JSONObject msg = new JSONObject();
        msg.put("type", "search_artist");
        msg.put("artist_name", artist_name);
        JSONObject response = communicate.exchange(msg);
        return (String) response.get("message");
    }

    public String searchAlbum(String album_name, String artist_name) {
        JSONObject msg = new JSONObject();
        msg.put("type", "search_album");
        msg.put("album_name", album_name);
        msg.put("artist_name", artist_name);
        JSONObject response = communicate.exchange(msg);
        return (String) response.get("message");
    }

    public String searchMusic(String music_name, String artist_name, String album_name) {
        JSONObject msg = new JSONObject();
        msg.put("type", "search_music");
        msg.put("music_name", music_name);
        msg.put("album_name", album_name);
        msg.put("artist_name", artist_name);
        JSONObject response = communicate.exchange(msg);
        return (String) response.get("message");
    }

    public void logout(double user_id) {
        this.notification.removeActiveUser(user_id);
    }

    public JSONObject downloadMusic(String artist_name, String album_name, String music_name) {
        JSONObject msg = new JSONObject();
        msg.put("type", "download_music");
        msg.put("artist_name", artist_name);
        msg.put("album_name", album_name);
        msg.put("music_name", music_name);
        JSONObject response = communicate.exchange(msg);
        System.out.println("DOWNLOAD");
        System.out.println(response);
        return response;
    }

    public JSONObject globalSearch(String term){
        JSONObject msg = new JSONObject();
        msg.put("type", "global_search");
        msg.put("global_term", term);
        JSONObject response = communicate.exchange(msg);
        return response;
    }

    public String getAlbumById(String albumId){
        JSONObject msg = new JSONObject();
        msg.put("type", "album_id");
        msg.put("id", albumId);
        JSONObject response = communicate.exchange(msg);
        return (String) response.get("message");
    }

    public String getArtistById (String artistId){
        JSONObject msg = new JSONObject();
        msg.put("type", "artist_id");
        msg.put("id", artistId);
        JSONObject response = communicate.exchange(msg);
        return (String) response.get("message");
    }

    public JSONObject uploadMusic(String artist_name, String album_name, String music_name, String filepath, int port) {
        JSONObject msg = new JSONObject();
        msg.put("type", "upload_music");
        msg.put("artist_name", artist_name);
        msg.put("album_name", album_name);
        msg.put("music_name", music_name);
        msg.put("port", Integer.toString(port));
        msg.put("filepath", filepath);
        try {
            msg.put("address", getClientHost());
        } catch (ServerNotActiveException e) {
            e.printStackTrace();
        }
        JSONObject response = communicate.exchange(msg);
        return response;
    }

    public void ping() {
    }

    public String givePowers(String username) {
        JSONObject msg = new JSONObject();
        msg.put("username", username);
        msg.put("type", "give_powers");
        JSONObject response = communicate.exchange(msg);
        if((boolean) response.get("success")) {
            System.out.println(response);
            JSONObject notif = new JSONObject();
            JSONArray user_ids = new JSONArray();
            user_ids.add((String) response.get("user_id"));
            notif.put("user_ids", user_ids);
            notif.put("message", (String) response.get("message"));
            notif.put("type", "give_powers");
            notification.sendNotification(notif);
        }
        return (String) response.get("message");
    }
}