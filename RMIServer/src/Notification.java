import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.io.IOException;
import java.lang.reflect.Array;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.util.ArrayList;
import java.util.Iterator;

public class Notification extends Thread {
    private static String multiCastAddress = "224.0.224.0";
    private static int multiCastSendPort = 3335;
    private ArrayList<JSONObject> tasks = new ArrayList<>();
    private ArrayList<Double> activeUsers = new ArrayList<>();

    public ArrayList<JSONObject> getTasks() {
        return tasks;
    }

    public void addActiveUser(double user_id) {
        this.activeUsers.add(user_id);
    }

    public void removeActiveUser(double user_id) {
        this.activeUsers.remove(this.activeUsers.indexOf(user_id));
    }

    public void sendNotification(JSONObject msg) {
        JSONArray user_ids = (JSONArray) msg.get("user_ids");
        msg.put("status", "up");
        JSONArray offline_users = new JSONArray();
        JSONObject task = new JSONObject();
        Iterator<String> iterator = user_ids.iterator();
        while (iterator.hasNext()) {
            double id = Double.parseDouble(iterator.next());
            if (!this.activeUsers.contains(id)) {
                offline_users.add(Double.toString(id));
            }
        }
        task.put("user_ids", offline_users);
        task.put("message", (String) msg.get("message"));
        this.tasks.add(task);
        try {
            send(msg);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String hasTasks(double user_id) {
        ArrayList<String> messages = new ArrayList<>();
        for (JSONObject _task : this.tasks) {
            JSONArray user_ids = (JSONArray) _task.get("user_ids");
            System.out.println(user_ids);
            Iterator<String> iterator = user_ids.iterator();
            while (iterator.hasNext()) {
                if (Double.parseDouble(iterator.next()) == user_id) {
                    messages.add((String) _task.get("message"));
                    iterator.remove();
                }
            }
        }

        return String.join("\n", messages);
    }

    private void send(JSONObject msg) throws IOException {
        byte[] buffer = msg.toString().getBytes();
        MulticastSocket socket = new MulticastSocket(Notification.multiCastSendPort);
        InetAddress group = InetAddress.getByName(Notification.multiCastAddress);
        DatagramPacket packet = new DatagramPacket(buffer, buffer.length, group, Notification.multiCastSendPort);
        socket.send(packet);
    }
}