import org.json.simple.JSONObject;

import java.rmi.AlreadyBoundException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;

public class RMIServer {
    public static void main(String[] args) {
        int port = 3333;
        try {
            RMIServerInterface rmiInterface = (RMIServerInterface) LocateRegistry.getRegistry(port).lookup("dropmusic");
            System.out.println("Secondary");
            System.out.println("RMIServer ready.");
            while(true){
                rmiInterface.ping();
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        } catch (RemoteException | NotBoundException e) {
            try {
                Server server = new Server();
                System.out.println("Primary");
            } catch (RemoteException | AlreadyBoundException e1) {
                e1.printStackTrace();
            }
        }
        System.out.println("RMIServer ready.");
    }
}
