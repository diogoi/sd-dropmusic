public class AlbumReview extends Model {
    private String rate;
    private String review;
    private double user_id;
    private double album_id;
    private String album_name;
    private String artist_name;

    public AlbumReview(Collection collection, String rate, String review, double user_id, String album_name, String artist_name) {
        super(collection);
        this.rate = rate;
        this.review = review;
        this.user_id = user_id;
        this.album_name = album_name;
        this.artist_name = artist_name;
    }

    @Override
    public String toString() {
        return " Review :" + review +
                "\n rate : " + rate + "\n";
    }

    public String albumReviewString(){
        return "User: " + getUser() +
                "Album: " + album_name +
                "Review :" + review +
                "rate : " + rate + "\n";
    }


    public String getReview() {
        return review;
    }

    public String getUser(){
        Collection users = this.collection.getController().getUsers();
        User user = (User) users.find(this.user_id);
        return user.getUsername();
    }


    /**
     * Runs all the album review validations
     * Returns true if the album was created successfully
     */
    @Override
    public boolean validations() {
        boolean errorFound = false;

        if (!Utils.isNumeric(this.rate)) {
            this.errors.add("Rate is not a number");
            errorFound = true;
        } else if (Integer.parseInt(this.rate) > 10 || Integer.parseInt(this.rate) < 0){
            this.errors.add("rate is not between 1-10");
            errorFound = true;
        }

        Artist artist = Artist.findByName(this.collection.getController().getArtists(), this.artist_name);
        if(artist == null) {
            this.errors.add("Artist doesnt exist");
            errorFound = true;
        } else {
            Album album = artist.findAlbum(this.album_name);
            if(album == null){
                this.errors.add("Album doesnt exist");
                errorFound = true;
            } else {
                this.album_id = album.getId();
            }
        }

        if(review.length() > 300) {
            this.errors.add("Revies has more than 300 characters");
            errorFound = true;
        }

        return !errorFound;
    }

    /**
     * Method to create associations between classes
     */
    @Override
    public void associations() {
        Collection users = this.collection.getController().getUsers();
        Collection albums = this.collection.getController().getAlbums();
        User user = (User) users.find(this.user_id);
        Album album = (Album) albums.find(this.album_id);
        album.addAlbumReviews(this.id);
        user.addAlbumReviews(this.id);
    }

    /**
     * Method to destroy associations between classes
     */
    @Override
    public void deleteAssociations() {
        Album album = (Album) this.collection.getController().getAlbums().find(this.album_id);
        User user = (User) this.collection.getController().getUsers().find(this.user_id);
        album.getAlbum_reviews().remove(album.getAlbum_reviews().indexOf(this.id));
        user.getAlbum_reviews().remove((user.getAlbum_reviews().indexOf(this.id)));
    }
}
