import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.Socket;

public class FileTransferUpload {

    public static void receive(String address, int port, String filename) {
        //Initialize socket
        Socket socket = null;
        byte[] contents = new byte[10000];

        //Initialize the FileOutputStream to the output file's full path.
        FileOutputStream fos = null;
        InputStream is = null;

        System.out.println("address: " + address);

        try {
            socket = new Socket(InetAddress.getByName(address), port);
            System.out.println("SOCKET ACCEPTED");
            fos = new FileOutputStream("1" + filename);
            is = socket.getInputStream();
        } catch (IOException e) {
            e.printStackTrace();
        }

        BufferedOutputStream bos = new BufferedOutputStream(fos);

        //No of bytes read in one read() call
        int bytesRead = 0;

        try {
            while ((bytesRead = is.read(contents)) != -1)
                bos.write(contents, 0, bytesRead);

            is.close();
            bos.flush();
            bos.close();
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("File saved successfully!");
    }
}

