import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Base64;

public class User extends Model {
    private boolean editor = false;
    private String username;
    private String password;
    private String dropbox_token;
    private String dropbox_acc_id;
    private ArrayList<Double> album_reviews = new ArrayList<>();


    public User(Collection collection, String username, String password) {
        super(collection);
        this.username = username;
        this.password = hashing(password);
        if(this.collection.getController().getUsers().getModels().isEmpty()){
            this.editor = true;
        }
    }

    public ArrayList<Double> getAlbum_reviews() {
        return album_reviews;
    }

    public boolean isEditor() {
        return editor;
    }

    public String getDropbox_acc_id() {
        return dropbox_acc_id;
    }

    public void setDropbox_acc_id(String dropbox_acc_id) {
        this.dropbox_acc_id = dropbox_acc_id;
    }

    public String getUsername() {
        return username;
    }

    public String getDropbox_token() {
        return dropbox_token;
    }

    public void setDropbox_token(String dropbox_token) {
        this.dropbox_token = dropbox_token;
    }

    public void addAlbumReviews(double album_review_id) {
        this.album_reviews.add(album_review_id);
    }

    /**
     * Method to encrypt the password
     */
    private String hashing(String password) {
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] hash = digest.digest(password.getBytes(Charset.forName("UTF-8")));
            return Base64.getEncoder().encodeToString(hash);
        } catch (NoSuchAlgorithmException ex) {
            System.out.println("password could't be hashed");
        }
        return password;
    }

    @Override
    public String toString() {
        return "User{" +
                "editor=" + editor +
                ", username='" + username + '\'' +
                ", dropbox_token='" + dropbox_token + '\'' +
                ", dropbox_acc_id='" + dropbox_acc_id + '\'' +
                ", album_reviews=" + album_reviews +
                '}';
    }

    /**
     * Check username and password
     */
    public boolean authorize(String username, String password) {
        System.out.println(username);
        System.out.println(password);
        System.out.println(hashing(password));
        System.out.println(this.username);
        System.out.println(this.password);
        System.out.println(this.username.equals(username) && this.password.equals(hashing(password)));
        return this.username.equals(username) && this.password.equals(hashing(password));
    }

    public void setEditor(boolean editor) {
        this.editor = editor;
    }
}