import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;

public class Receiver {
    private String multicastAddress = "224.0.224.0";
    private int multiCastReceivePort = 3333;
    private MulticastSocket socket = null;
    private Controller controller = new Controller();

    /**
     * Init Receiver
     */
    public void start() {
        try {
            joinGroup();
            //noinspection InfiniteLoopStatement
            while (true) {
                Router router = new Router(this.controller, getMessage());
                new Thread(router).start();
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            socket.close();
        }
    }



    private void joinGroup() throws IOException {
        this.socket = new MulticastSocket(this.multiCastReceivePort);
        InetAddress group = InetAddress.getByName(this.multicastAddress);
        socket.joinGroup(group);
    }

    /**
     * Get JSON Object and parse it
     * Returns
     */
    private JSONObject getMessage() throws IOException {
        byte[] buffer = new byte[1024];
        DatagramPacket packet = new DatagramPacket(buffer, buffer.length);
        socket.receive(packet);
        String receivedMessage = new String(packet.getData(), 0, packet.getLength());
        JSONParser parser = new JSONParser();
        JSONObject receivedMessageObj = null;
        try {
            Object obj = parser.parse(receivedMessage);
            receivedMessageObj = (JSONObject) obj;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        receivedMessageObj.put("address", packet.getAddress().getHostAddress());
        System.out.println("PARAMS");
        System.out.println(packet.getAddress().getHostAddress());
        System.out.println(receivedMessageObj.toString());
        return receivedMessageObj;
    }
}