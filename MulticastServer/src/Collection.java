import java.util.ArrayList;
import java.util.Iterator;

public class Collection {
    private long id = 0;
    private Controller controller;
    private ArrayList<Model> models = new ArrayList<>();
    private final Object lock = new Object();

    public Collection(Controller controller) {
        this.controller = controller;
    }

    public Controller getController() {
        return controller;
    }

    public Model find(double id) {
        for (Model m : models) {
            if (m.getId() == id) {
                return m;
            }
        }
        return null;
    }

    public ArrayList<Model> getModels() {
        return models;
    }

    @Override
    public String toString() {
        return "Collection{" +
                "models=" + models +
                '}';
    }

    /**
     * Method to add a new Model to the list
     */
    public void save(Model model) {
        synchronized (this.lock) {
            model.setId(this.id);
            this.id += 1;
            this.models.add(model);
        }
    }

    /**
     * Method to delete a model by its id
     */
    public void delete(double id) {
        Iterator<Model> itr = this.models.iterator();
        while (itr.hasNext()) {
            Model model = itr.next();
            if(model.id == id){
                itr.remove();
            }
        }
    }
}