import java.util.ArrayList;

public class Model {
    protected double id;
    protected Collection collection;
    protected ArrayList<String> errors = new ArrayList<>();

    public Model(Collection collection) {
        this.collection = collection;
    }

    public ArrayList<String> getErrors() {
        return errors;
    }

    public double getId() {
        return id;
    }

    public void setId(double id) {
        this.id = id;
    }

    public boolean validations(){
        return true;
    }

    public void associations() {};

    public void deleteAssociations() {};

    /**
     * Method to validate and save a new model
     */
    public boolean save() {
        if (this.validations()) {
            this.collection.save(this);
            this.associations();
            return true;
        }
        return false;
    }

    /**
     * Method to delete a model and its associations
     */
    public void delete() {
        this.deleteAssociations();
        this.collection.delete(this.id);
    }
}