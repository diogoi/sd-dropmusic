import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.io.File;


/**
 * Class with the methods to change the data and do all the operations
 */
public class Controller {
    private Collection users = new Collection(this);
    private Collection artists = new Collection(this);
    private Collection albums = new Collection(this);
    private Collection musics = new Collection(this);
    private Collection album_reviews = new Collection(this);

    public Collection getUsers() {
        return users;
    }

    public Collection getArtists() {
        return artists;
    }

    public Collection getAlbums() {
        return albums;
    }

    public Collection getMusics() {
        return musics;
    }

    public Collection getAlbum_reviews() {
        return album_reviews;
    }

    public JSONObject register(String username, String password) {
        User user = new User(this.users, username, password);
        user.save();
        JSONObject response = new JSONObject();
        response.put("message", "Successfull Register");
        return response;
    }

    public JSONObject download_music(String artist_name, String album_name, String music_name, String address){
        JSONObject response = new JSONObject();
        Artist _artist = null;
        for (Model artist : this.artists.getModels()) {
            if (((Artist) artist).getName().equals(artist_name)) {
                _artist = (Artist) artist;
                break;
            }
        }

        Music music = null;

        if(_artist != null) {
            music = _artist.findMusic(music_name, album_name);
        } else {
            response.put("success", false);
            response.put("message", "Artist not found");
        }

        if(music != null) {
            FileTransferDownload FT = new FileTransferDownload(music.getPath());
            new Thread(FT).start();
            response.put("success", true);
            response.put("port", Integer.toString(FT.getPort()));
            response.put("address", address);
            response.put("filename", new File(music.getPath()).getName());
            response.put("message", "Downloading Music");
        } else {
            response.put("success", false);
            response.put("message", "Music not found");
        }

        return response;
    }

    public JSONObject upload_music(String artist_name, String album_name, String music_name, String port, String address, String filepath){
        JSONObject response = new JSONObject();
        Artist _artist = null;
        for (Model artist : this.artists.getModels()) {
            if (((Artist) artist).getName().equals(artist_name)) {
                _artist = (Artist) artist;
                break;
            }
        }

        Music music = null;

        if(_artist != null) {
            music = _artist.findMusic(music_name, album_name);
        } else {
            response.put("success", false);
            response.put("message", "Artist not found");
        }

        if(music != null) {
            if(music.getPath() == null) {
                int _port = Integer.parseInt(port);
                FileTransferUpload.receive(address, _port, filepath);
                music.setPath(filepath);
                response.put("success", true);
                response.put("message", "Uploading Music");
            } else {
                response.put("success", false);
                response.put("message", "Music already has a file");
            }
        } else {
            response.put("success", false);
            response.put("message", "Music not found");
        }

        return response;
    }



    public JSONObject login(String username, String password) {
        JSONObject response = new JSONObject();
        for (Model user : this.users.getModels()) {
            User temp_user = (User) user;
            if (temp_user.authorize(username, password)) {
                response.put("success", true);
                response.put("message", "Authenticated Successfully");
                response.put("user_id", temp_user.getId());
                response.put("editor", temp_user.isEditor());
                return response;
            }
        }
        response.put("success", false);
        response.put("message", "Authentication failed");
        return response;
    }

    public JSONObject login_dropbox(String token, String acc_id) {
        JSONObject response = new JSONObject();
        for (Model user : this.users.getModels()) {
            User temp_user = (User) user;
            System.out.println("TEMP USER: " + temp_user);
            System.out.println("ACC_ID: " + acc_id);
            if (temp_user.getDropbox_acc_id().equals(acc_id)){
                response.put("success", true);
                response.put("message", "Authenticated Successfully");
                response.put("user_id", temp_user.getId());
                response.put("editor", temp_user.isEditor());
                return response;
            }
        }
        response.put("success", false);
        response.put("message", "Authentication failed");
        return response;
    }

    public JSONObject create_artist(String name) {
        Artist artist = new Artist(this.artists, name);
        artist.save();
        JSONObject response = new JSONObject();
        response.put("message", "Artist Created");
        return response;
    }

    public JSONObject create_album(String name, String artist, String info, double user_id) {
        Album album = new Album(this.albums, name, artist, info, user_id);
        JSONObject response = new JSONObject();
        if (album.save()) {
            response.put("message", "Album created");
        } else {
            response.put("message", String.join(", ", album.getErrors()));
        }
        return response;
    }

    public JSONObject create_music(String name, String artist, String album, String url) {
        Music music = new Music(this.musics, name, artist, album, url);
        JSONObject response = new JSONObject();
        if (music.save()) {
            response.put("message", "Music created");
        } else {
            response.put("message", String.join(", ", music.getErrors()));
        }
        return response;
    }

    public JSONObject delete_music(String music_name, String album_name, String artist_name) {
        JSONObject response = new JSONObject();
        Artist _artist = null;
        for (Model artist : this.artists.getModels()) {
            if (((Artist) artist).getName().equals(artist_name)) {
                _artist = (Artist) artist;
                break;
            }
        }
        Music music = _artist.findMusic(music_name, album_name);

        if (music == null) {
            response.put("message", "Music doesnt exist");
        } else {
            music.delete();
            response.put("message", "Music deleted");
        }
        return response;
    }

    public JSONObject delete_album(String album_name, String artist_name) {
        JSONObject response = new JSONObject();
        Artist _artist = null;
        for (Model artist : this.artists.getModels()) {
            if (((Artist) artist).getName().equals(artist_name)) {
                _artist = (Artist) artist;
                break;
            }
        }

        Album album = _artist.findAlbum(album_name);

        if (album == null) {
            response.put("message", "Album doesnt exist");
        } else {
            album.delete();
            response.put("message", "Album deleted");
        }
        return response;
    }

    public JSONObject delete_artist(String artist_name) {
        JSONObject response = new JSONObject();
        Artist _artist = null;
        for (Model artist : this.artists.getModels()) {
            if (((Artist) artist).getName().equals(artist_name)) {
                _artist = (Artist) artist;
                break;
            }
        }

        if (_artist == null) {
            response.put("message", "Artist doesnt exist");
        } else {
            _artist.delete();
            response.put("message", "Artist deleted");
        }
        return response;
    }

    public JSONObject write_album_review(String artist, String album, String rate, String review, double user_id) {
        AlbumReview album_review = new AlbumReview(this.album_reviews, rate, review, user_id, album, artist);
        JSONObject response = new JSONObject();
        if (album_review.save()) {
            response.put("message", "Album review created");
        } else {
            response.put("message", String.join(", ", album_review.getErrors()));
        }
        return response;
    }

    public JSONObject edit_music(String music_name, String artist_name, String new_music_name, String album_name) {
        JSONObject response = new JSONObject();
        Artist _artist = null;
        for (Model artist : this.artists.getModels()) {
            if (((Artist) artist).getName().equals(artist_name)) {
                _artist = (Artist) artist;
                break;
            }
        }

        if(_artist == null){
            response.put("message", "Artist doesnt exist");
            return response;
        }

        else {
            Music music = _artist.findMusic(music_name, album_name);

            if (music == null) {
                response.put("message", "Music doesnt exist");
            } else {
                music.setName(new_music_name);
                response.put("message", "Music updated");
            }
            return response;
        }
    }

    public JSONObject edit_artist(String artist_name, String new_artist_name) {
        JSONObject response = new JSONObject();
        Artist _artist = null;
        for (Model artist : this.artists.getModels()) {
            if (((Artist) artist).getName().equals(artist_name)) {
                _artist = (Artist) artist;
                break;
            }
        }

        if (_artist == null) {
            response.put("message", "Artist doesnt exist");
        } else {
            _artist.setName(new_artist_name);
            response.put("message", "Artist edited");
        }
        return response;
    }

    public JSONObject edit_album(String album_name, String new_album_name, String artist_name, String new_info, double user_id) {
        JSONObject response = new JSONObject();
        Artist _artist = null;
        for (Model artist : this.artists.getModels()) {
            if (((Artist) artist).getName().equals(artist_name)) {
                _artist = (Artist) artist;
                break;
            }
        }

        Album album = null;
        if(_artist != null) {
            album = _artist.findAlbum(album_name);
        }

        if (album == null || _artist == null) {
            response.put("message", "Album doesnt exist");
            response.put("success", false);
        } else {
            album.setName(new_album_name);
            album.setInfo(new_info);
            album.addUser(user_id);
            
            response.put("message", "Album updated");
            JSONArray user_ids = new JSONArray();
            for(double id : album.getUser_edits()){
                user_ids.add(Double.toString(id));
            }
            response.put("success", true);
            response.put("user_ids", user_ids);
        }
        return response;
    }

    public JSONObject search_music(String music_name, String album_name, String artist_name) {
        JSONObject response = new JSONObject();
        Artist _artist = null;
        for (Model artist : this.artists.getModels()) {
            if (((Artist) artist).getName().equals(artist_name)) {
                _artist = (Artist) artist;
                break;
            }
        }
        Music music = _artist.findMusic(music_name, album_name);

        if (music == null) {
            response.put("message", "Music doesnt exist");
            response.put("success", false);
        } else {
            response.put("message", music.toString());
            response.put("success", true);
            response.put("url", music.getUrl());
        }
        return response;
    }

    public JSONObject search_album(String album_name, String artist_name) {
        JSONObject response = new JSONObject();
        Artist _artist = null;
        for (Model artist : this.artists.getModels()) {
            if (((Artist) artist).getName().equals(artist_name)) {
                _artist = (Artist) artist;
                break;
            }
        }

        Album album = _artist.findAlbum(album_name);

        if (album == null) {
            response.put("message", "Album doesnt exist");
            response.put("success", false);
        } else {
            response.put("message", album.toString());
            response.put("success", true);
        }
        return response;
    }

    public JSONObject search_artist(String artist_name) {
        JSONObject response = new JSONObject();
        Artist _artist = null;
        for (Model artist : this.artists.getModels()) {
            if (((Artist) artist).getName().equals(artist_name)) {
                _artist = (Artist) artist;
                break;
            }
        }

        if (_artist == null) {
            response.put("message", "Artist doesnt exist");
            response.put("success", false);
        } else {
            response.put("message", _artist.toString());
            response.put("success", true);
        }
        return response;
    }

    public JSONObject global_search(String global_term) {
        JSONObject response = new JSONObject();
        response.put("album", Utils.albumPartialResults(albums, global_term));
        response.put("artist", Utils.artistPartialResults(getArtists(), global_term));
        return response;
    }

    public JSONObject album_by_id(String id) {
        JSONObject response = new JSONObject();
        double albumId = Double.parseDouble(id);
        Album _album = null;
        for (Model album: this.albums.getModels()) {
            if (((Album) album).getId() == (albumId)) {
                _album = (Album) album;
                break;
            }
        }

        if (_album== null) {
            response.put("message", "Album doesnt exist");
        } else {
            response.put("message", _album.toString());
        }
        return response;

    }

    public JSONObject artist_by_id(String id) {
        JSONObject response = new JSONObject();
        double artistId= Double.parseDouble(id);
        Artist _artist = null;
        for (Model artist : this.artists.getModels()) {
            if (((Artist) artist).getId() == (artistId)) {
                _artist = (Artist) artist;
                break;
            }
        }
        if (_artist == null) {
            response.put("message", "Artist doesnt exist");
        } else {
            response.put("message", _artist.toString());
        }
        return response;
    }

    public JSONObject give_powers(String username) {
        JSONObject response = new JSONObject();
        for (Model user : this.users.getModels()) {
            User temp_user = (User) user;
            if (temp_user.getUsername().equals(username)) {
                response.put("success", true);
                response.put("message", "User promoted");
                response.put("user_id", Double.toString(temp_user.getId()));
                response.put("editor", temp_user.isEditor());
                temp_user.setEditor(true);
                return response;
            }
        }
        response.put("success", false);
        response.put("message", "User doesnt exist");
        return response;
    }

    public JSONObject associate_dropbox(double user_id, String token, String acc_id) {
        JSONObject response = new JSONObject();
        User user = (User) this.users.find(user_id);

        if(user != null) {
            user.setDropbox_token(token);
            user.setDropbox_acc_id(acc_id);
        }
        System.out.println(user);

        return response;
    }
}