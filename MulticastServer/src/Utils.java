import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Utils {
    public static boolean matches(String regex, String input){
        Pattern pattern = Pattern.compile(regex);
        Matcher m = pattern.matcher(input);
        if(m.find()){
            return true;
        };
        return false;
    }

    public static boolean isNumeric(String strNum) {
        return strNum.matches("-?\\d+(\\.\\d+)?");
    }

    public static String artistPartialResults(Collection artists, String global_term) {
        String results = new String();
        System.out.println(artists.getModels());
        System.out.println(artists.getModels().size());
        for (Model artist: artists.getModels()) {
            System.out.println("teste");
            if (((Artist) artist).getName().contains(global_term)) {
                results +=  ((Artist) artist).getId() +  "-" + ((Artist) artist).getName() + ",";
            }
        }
        return results;
    }

    public static String albumPartialResults(Collection albums, String global_term) {
        String results = new String();
        for (Model album: albums.getModels()) {
            if (((Album) album).getName().contains(global_term)) {
                results +=  ((Album) album).getId() + "-" + ((Album) album).getName();
                break;
            }
        }
        return results;
    }

}