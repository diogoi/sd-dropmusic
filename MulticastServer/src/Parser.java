import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Parser {
    public static HashMap<String, Object> toHash(String message) {
        // The protocol message is going to be parsed into key-value map
        HashMap<String, Object> protocolHash = new HashMap<>();

        // Clean protocol message of whitespaces
        String[] splittedMessage = message.split("[|;]");
        String[] cleanMessage = Arrays.stream(splittedMessage).map(String::trim).toArray(String[]::new);


        for (int i = 0; i < cleanMessage.length; i += 2) {
            if(Utils.matches("_count$", cleanMessage[i])) {
                Pattern pattern = Pattern.compile(".*(?=_count$)");
                Matcher m = pattern.matcher(cleanMessage[i]);
                m.find();
                String key = m.group();
                int lowerBound = i+2;
                int upperBound = i+2+2*Integer.parseInt(cleanMessage[i+1]);
                protocolHash.put(key, stringToArray(Arrays.copyOfRange(cleanMessage, lowerBound, upperBound)));
                i += upperBound - lowerBound;
            } else {
                protocolHash.put(cleanMessage[i], cleanMessage[i + 1]);
            }
        }
        return protocolHash;
    }

    private static String[] stringToArray(String[] protocolArray) {
        String[] finalArray = new String[protocolArray.length / 2];

        for (int i = 0; i < protocolArray.length; i += 2) {
            Pattern pattern = Pattern.compile("(?<=_).(?=_)");
            List<String> matches = new ArrayList<String>();
            Matcher m = pattern.matcher(protocolArray[i]);
            while (m.find()) {
                matches.add(m.group());
            }
            finalArray[Integer.parseInt(matches.get(0))] = protocolArray[i+1];
        }
        return finalArray;
    }

    /**
     * Creates a JSON Object with the string input
     */
    public static JSONObject stringToObject(String message) throws ParseException {
        JSONParser parser = new JSONParser();
        Object obj = parser.parse(message);
        return (JSONObject) obj;
    }

    public static String ObjectToString(JSONObject obj){
        return obj.toString();
    }
}