import org.json.simple.JSONObject;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;

public class Sender {
    private static String multiCastAddress = "224.0.224.0";
    private static int multiCastSendPort = 3334;

    /**
     * Method to send information to the Client
     */
    public static void send(JSONObject msg, Long id) throws IOException {
        System.out.println("SENDER");
        System.out.println(msg);
        msg.put("message_id", id);
        byte[] buffer = msg.toString().getBytes();
        MulticastSocket socket = new MulticastSocket(Sender.multiCastSendPort);
        InetAddress group = InetAddress.getByName(Sender.multiCastAddress);
        DatagramPacket packet = new DatagramPacket(buffer, buffer.length, group, Sender.multiCastSendPort);
        socket.send(packet);
    }
}