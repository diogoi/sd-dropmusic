import java.util.ArrayList;

public class Music extends Model{
    private String name, artist_name, album_name;
    private double album_id;
    private String path;
    private String url;


    public Music(Collection collection, String name, String artist_name, String album_name, String url) {
        super(collection);
        this.name = name;
        this.artist_name = artist_name;
        this.album_name = album_name;
        this.url = url;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    @Override
    public String toString() {
        return "Music{" +
                "name='" + name + '\'' +
                ", artist_name='" + artist_name + '\'' +
                ", album_name='" + album_name + '\'' +
                ", album_id=" + album_id +
                ", id=" + id +
                '}';
    }

    public String getName() {
        return name;
    }


    public String getUrl() {
        return url;
    }

    /**
     * Runs all the song validations
     * Returns true if the song was created successfully
     */
    @Override
    public boolean validations() {
        boolean errorFound = false;
        Controller controller = this.collection.getController();
        Collection artists = controller.getArtists();
        Collection albums = controller.getAlbums();

        Artist _artist = Artist.findByName(artists, this.artist_name);
        ArrayList<Album> _albums = Album.findByName(albums, this.album_name);

        if(_artist == null) {
            this.errors.add("Artist doesnt exist");
            errorFound = true;
        }

        if(_albums == null) {
            this.errors.add("Album doesnt exist");
            errorFound = true;
        }

        if(_artist != null && _albums != null) {
            boolean found = false;
            for (Album album : _albums) {
                if (album.getArtist_id() == _artist.getId()) {
                    this.album_id = album.getId();
                    found = true;
                }
            }
            if (!found) {
                this.errors.add("Artist doesnt own this album");
                errorFound = true;
            }
        }

        if(errorFound){
            return false;
        } else {
            return true;
        }
    }
    /**
     * Method to create associations between classes
     */
    @Override
    public void associations() {
        Collection albums = this.collection.getController().getAlbums();
        Album album = (Album) albums.find(this.album_id);
        album.addMusic(this.id);
    }

    /**
     * Method to destroy associations between classes
     */
    @Override
    public void deleteAssociations() {
        Album album = (Album) this.collection.getController().getAlbums().find(this.album_id);
        album.getMusics().remove(album.getMusics().indexOf(this.id));
    }

    public void setPath(String path) {
        this.path = path;
    }

}