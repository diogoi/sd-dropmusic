import java.util.ArrayList;

public class Artist extends Model {
    private ArrayList<Double> albums = new ArrayList<>();
    private String name;

    public Artist(Collection collection, String name) {
        super(collection);
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return " Artist Name: " + name +
                "\n albums=" + getAlbumTitles();
    }

    public void addAlbum(double id) {
        this.albums.add(id);
    }

    public ArrayList<Double> getAlbums() {
        return albums;
    }

    /**
     * Gets all the album titles of an artist
     * Returns a String with that information
     */
    public String getAlbumTitles(){
        String output = new String();
        Collection albumCollection = this.collection.getController().getAlbums();
        for (double albumId: this.albums) {
            Album album= (Album) albumCollection.find(albumId);
            output += album.getName();
        }
        return output;
    }



    /**
     * Method to find an album by its name
     * Returns a list of albums, if they exist
     */
    public static Artist findByName(Collection collection, String name) {
        for (Model artist : collection.getModels()) {
            Artist _artist = (Artist) artist;
            if (_artist.getName().equals(name)) {
                return _artist;
            }
        }
        return null;
    }

    /**
     * Method to destroy associations between classes
     */
    public void deleteAssociations() {
        Collection albums = this.collection.getController().getAlbums();

        while (!this.albums.isEmpty()) {
            Album album = (Album) albums.find(this.albums.get(0));
            album.delete();
        }
    }

    /**
     * Method to find a music by its name
     * Return a Music object
     */
    public Music findMusic(String music_name, String album_name) {
        for (double album_id : this.albums) {
            Album album = (Album) this.collection.getController().getAlbums().find(album_id);
            if (album.getName().equals(album_name)) {
                for (double music_id : album.getMusics()) {
                    Music music = (Music) this.collection.getController().getMusics().find(music_id);
                    if (music.getName().equals(music_name)) {
                        return music;
                    }
                }
            }
        }

        return null;
    }

    /**
     * Method to find a album by its name
     * Return a Album object
     */
    public Album findAlbum(String album_name) {
        for (double album_id : this.albums) {
            Album album = (Album) this.collection.getController().getAlbums().find(album_id);
            if (album.getName().equals(album_name)) {
                return album;
            }
        }
        return null;
    }
}
