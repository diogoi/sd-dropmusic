import org.json.simple.JSONObject;

import java.io.IOException;

public class Router implements Runnable {
    private JSONObject receivedMessageObj;
    private Controller controller;

    public Router(Controller controller, JSONObject receivedMessageObj) {
        this.controller = controller;
        this.receivedMessageObj = receivedMessageObj;
    }

    /**
     * Method where the parsed information is processed
     */
    public void run() {
        JSONObject response = new JSONObject();
        switch ((String) receivedMessageObj.get("type")) {
            case "test":
                System.out.println("RMIServer connected");
                response.put("message", "Connected to MultiCastSever");
                break;
            case "register":
                response = this.controller.register((String) receivedMessageObj.get("username"), (String) receivedMessageObj.get("password"));
                break;

            case "login":
                response = this.controller.login((String) receivedMessageObj.get("username"), (String) receivedMessageObj.get("password"));
                break;

            case "create_artist":
                response = this.controller.create_artist((String) receivedMessageObj.get("name"));
                break;

            case "create_album":
                response = this.controller.create_album((String) receivedMessageObj.get("name"), (String) receivedMessageObj.get("artist"), (String) receivedMessageObj.get("info"), (double) receivedMessageObj.get("user_id"));
                break;

            case "create_music":
                response = this.controller.create_music((String) receivedMessageObj.get("name"), (String) receivedMessageObj.get("artist"), (String) receivedMessageObj.get("album"), (String) receivedMessageObj.get("url"));
                break;

            case "delete_music":
                response = this.controller.delete_music((String) receivedMessageObj.get("music_name"), (String) receivedMessageObj.get("album_name"), (String) receivedMessageObj.get("artist_name"));
                break;

            case "delete_album":
                response = this.controller.delete_album((String) receivedMessageObj.get("album_name"), (String) receivedMessageObj.get("artist_name"));
                break;

            case "delete_artist":
                response = this.controller.delete_artist((String) receivedMessageObj.get("artist_name"));
                break;

            case "write_album_review":
                response = this.controller.write_album_review((String) receivedMessageObj.get("artist_name"), (String) receivedMessageObj.get("album_name"), (String) receivedMessageObj.get("rate"), (String) receivedMessageObj.get("review"), (double) receivedMessageObj.get("user_id"));
                break;

            case "edit_artist":
                response = this.controller.edit_artist((String) receivedMessageObj.get("artist_name"), (String) receivedMessageObj.get("new_artist_name"));
                break;

            case "edit_album":
                response = this.controller.edit_album((String) receivedMessageObj.get("album_name"), (String) receivedMessageObj.get("new_album_name"),  (String) receivedMessageObj.get("artist_name"), (String) receivedMessageObj.get("new_info"), (double) receivedMessageObj.get("user_id"));
                break;

            case "edit_music":
                response = this.controller.edit_music((String) receivedMessageObj.get("music_name"), (String) receivedMessageObj.get("artist_name"), (String) receivedMessageObj.get("new_music_name"),  (String) receivedMessageObj.get("album_name"));
                break;

            case "search_music":
                response = this.controller.search_music((String) receivedMessageObj.get("music_name"), (String) receivedMessageObj.get("album_name"), (String) receivedMessageObj.get("artist_name"));
                break;

            case "search_album":
                response = this.controller.search_album((String) receivedMessageObj.get("album_name"), (String) receivedMessageObj.get("artist_name"));
                break;

            case "search_artist":
                response = this.controller.search_artist((String) receivedMessageObj.get("artist_name"));
                break;

            case "global_search":
                response = this.controller.global_search((String) receivedMessageObj.get("global_term"));
                break;

            case "album_id":
                response = this.controller.album_by_id((String) receivedMessageObj.get("id"));
                break;

            case "artist_id":
                response = this.controller.artist_by_id((String) receivedMessageObj.get("id"));
                break;


            case "download_music":
                response = this.controller.download_music((String) receivedMessageObj.get("artist_name"), (String) receivedMessageObj.get("album_name"), (String) receivedMessageObj.get("music_name"), (String) receivedMessageObj.get("address"));
                break;

            case "upload_music":
                response = this.controller.upload_music((String) receivedMessageObj.get("artist_name"), (String) receivedMessageObj.get("album_name"), (String) receivedMessageObj.get("music_name"), (String) receivedMessageObj.get("port"), (String) receivedMessageObj.get("address"), (String) receivedMessageObj.get("filepath"));
                break;

            case "give_powers":
                response = this.controller.give_powers((String) receivedMessageObj.get("username"));
                break;

            case "associate_dropbox":
                response = this.controller.associate_dropbox((double) receivedMessageObj.get("user_id"), (String) receivedMessageObj.get("token"), (String) receivedMessageObj.get("acc_id"));
                break;

            case "login_dropbox":
                response = this.controller.login_dropbox((String) receivedMessageObj.get("token"), (String) receivedMessageObj.get("acc_id"));
                break;

        }


        System.out.println("\n----------------------------\n");
        System.out.println("Users");
        System.out.println(this.controller.getUsers());
        System.out.println("Artists");
        System.out.println(this.controller.getArtists());
        System.out.println("Albums");
        System.out.println(this.controller.getAlbums());
        System.out.println("Musics");
        System.out.println(this.controller.getMusics());
        System.out.println("Album Reviews");
        System.out.println(this.controller.getAlbum_reviews());
        System.out.println("\n----------------------------\n");

        System.out.println("RESPONSE");
        System.out.println(response);

        try {
            Sender.send(response, (Long) receivedMessageObj.get("message_id"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}