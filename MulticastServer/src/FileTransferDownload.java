import java.io.*;
import java.net.*;

public class FileTransferDownload implements Runnable {
    //Initialize Sockets
    private int port = 5000;
    ServerSocket ssock;
    private String filepath;

    public FileTransferDownload(String filepath) {
        this.port = 5000;
        this.filepath = filepath;

        while (true) {
            try {
                this.ssock = new ServerSocket(this.port);
                break;
            } catch (IOException e) {
                port += 1;
            }
        }
    }

    public int getPort() {
        return port;
    }

    public void run() {
        System.out.println("Waiting to connect...");
        Socket socket = null;
        try {
            socket = this.ssock.accept();
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("Start transfer....");

        File file = new File(filepath);
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        BufferedInputStream bis = new BufferedInputStream(fis);

        //Get socket's output stream
        OutputStream os = null;
        try {
            os = socket.getOutputStream();
        } catch (IOException e) {
            e.printStackTrace();
        }

        //Read File Contents into contents array
        byte[] contents;
        long fileLength = file.length();
        long current = 0;

        long start = System.nanoTime();


        try {
            while (current != fileLength) {
                int size = 10000;
                if (fileLength - current >= size)
                    current += size;
                else {
                    size = (int) (fileLength - current);
                    current = fileLength;
                }
                contents = new byte[size];
                bis.read(contents, 0, size);
                os.write(contents);
                System.out.print("Sending file ... " + (current * 100) / fileLength + "% complete!");
            }
            os.flush();
            socket.close();
            ssock.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("File sent succesfully!");
    }
}

