import java.util.ArrayList;

public class Album extends Model {
    private ArrayList<Double> musics = new ArrayList<>();
    private ArrayList<Double> album_reviews = new ArrayList<>();
    private ArrayList<Double> user_edits = new ArrayList<>();
    private double artist_id;
    private String name;
    private String info;
    private String artist_name;

    public Album(Collection collection, String name, String artist_name, String info, double user_id) {
        super(collection);
        this.name = name;
        this.info = info;
        this.artist_name = artist_name;
        this.user_edits.add(user_id);
    }

    public ArrayList<Double> getMusics() {
        return musics;
    }

    public String getName() {
        return name;
    }

    public double getArtist_id() {
        return artist_id;
    }

    public ArrayList<Double> getUser_edits() {
        return user_edits;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public ArrayList<Double> getAlbum_reviews() {
        return album_reviews;
    }

    public void addUser(double user_id){
        this.user_edits.add(user_id);
    }

    @Override
    public String toString() {
        return "Album Name:" +  name + "\n Artist : " + artist_name + "\n Songs : [ " + this.getMusicTitles() + "\n Description: " + info + "\n Reviews : [" + this.getReviews() + "]";
    }

    public void addAlbumReviews(double album_review_id) {
        this.album_reviews.add(album_review_id);
    }

    /**
     * Method to find an album by its name
     * Returns a list of albums, if they exist
     */
    public static ArrayList<Album> findByName(Collection collection, String name) {
        ArrayList<Album> albums = new ArrayList<>();

        for (Model album : collection.getModels()) {
            Album _album = (Album) album;
            if (_album.getName().equals(name)) {
                albums.add(_album);
            }
        }

        if (albums.isEmpty()) {
            return null;
        } else {
            return albums;
        }
    }

    /**
     * Get all the reviews of an album
     * Returns a String with the text
     */

    public String getReviews(){
        String output = new String();
        Collection albumReviewCollection = this.collection.getController().getAlbum_reviews();
        for (double reviewId : this.album_reviews ) {
            AlbumReview albumReview = (AlbumReview) albumReviewCollection.find(reviewId);
            output += albumReview.toString();
        }
        return output;
    }

    public String getMusicTitles(){
        String output = new String();
        Collection musicCollection = this.collection.getController().getMusics();
        for (double musicId : this.musics) {
            Music music = (Music) musicCollection.find(musicId);
            output += music.getName();
        }
        return output;
    }



    public void addMusic(double id) {
        this.musics.add(id);
    }


    /**
     * Runs all the album validations
     * Returns true if the album was created successfully
     */
    @Override
    public boolean validations() {
        Collection artists = this.collection.getController().getArtists();
        Artist _artist = Artist.findByName(artists, this.artist_name);
        if (_artist == null) {
            this.errors.add("Artist doesnt exist");
            return false;
        }
        this.artist_id = _artist.id;
        return true;
    }

    /**
     * Method to create associations between classes
     */
    @Override
    public void associations() {
        Collection artists = this.collection.getController().getArtists();
        Artist artist = (Artist) artists.find(this.artist_id);
        artist.addAlbum(this.id);
    }

    /**
     * Method to destroy associations between classes
     */
    @Override
    public void deleteAssociations() {
        Artist artist = (Artist) this.collection.getController().getArtists().find(this.artist_id);
        artist.getAlbums().remove(artist.getAlbums().indexOf(this.id));
        Collection musics = this.collection.getController().getMusics();
        Collection album_reviews = this.collection.getController().getAlbum_reviews();


        while (!this.musics.isEmpty()) {
            Music music = (Music) musics.find(this.musics.get(0));
            music.delete();
        }

        while (!this.album_reviews.isEmpty()) {
            AlbumReview album_review = (AlbumReview) album_reviews.find(this.album_reviews.get(0));
            album_review.delete();
        }
    }
}
