<%--
  Created by IntelliJ IDEA.
  User: diogo
  Date: 16/12/2018
  Time: 16:46
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>

<jsp:include page="../navbar.jsp"/>
<h1>Search Album</h1>

<s:form action="create_album" method="post">
    <s:textfield name="artist_name" label="Artist Name"/>
    <s:textfield name="album_name" label="Album Name"/>
    <s:textfield name="song_name" label="Song Name"/>
    <s:submit type="submit"/>
</s:form>

</body>
</html>
