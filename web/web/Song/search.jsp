<%--
  Created by IntelliJ IDEA.
  User: diogo
  Date: 16/12/2018
  Time: 16:43
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<html>
<head>
    <title>Title</title>
</head>
<body>

<jsp:include page="../navbar.jsp"/>
<h1>Search Song</h1>

<s:form action="/show_song" >
    <s:textfield name="songBean.artist_name" label="Artist name"/>
    <s:textfield name="songBean.album_name" label="Album name"/>
    <s:textfield name="songBean.song_name" label="Song name"/>
    <s:submit />
</s:form>

</body>
</html>
