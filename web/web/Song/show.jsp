<%@ taglib prefix="s" uri="/struts-tags" %>
<%--
  Created by IntelliJ IDEA.
  User: diogo
  Date: 15/12/2018
  Time: 21:56
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<p>Name: <s:property value="songBean.song_name" /></p>
<p>Album: <s:property value="songBean.album_name" /></p>
<p>Artist: <s:property value="songBean.artist_name" /></p>

<audio controls autoplay>
    <source src="<s:property value="songBean.url"/>">
    Your browser does not support the audio element.
</audio>

</body>
</html>
