<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>

<jsp:include page="../navbar.jsp"/>

<h1>Create Song</h1>

<s:form action="/create_song" >
    <s:textfield name="songBean.artist_name" label="Artist name"/>
    <s:textfield name="songBean.album_name" label="Album name"/>
    <s:textfield name="songBean.song_name" label="Song name"/>
    <s:textfield name="songBean.url" label="URL"/>
    <s:submit />
</s:form>

</body>
</html>
