<%--
  Created by IntelliJ IDEA.
  User: diogo
  Date: 15/12/2018
  Time: 16:03
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Artist</title>
</head>
<body>

<jsp:include page="../navbar.jsp"/>
<h1>Artist Info</h1>
<p><s:property value="show_info"/></p>

</body>
</html>
