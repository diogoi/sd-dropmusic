<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Register</title>
</head>
<body>
    <h3>Register for a prize by completing this form.</h3>

    <s:form action="/create_register">
        <s:textfield name="userBean.username" label="Username" />
        <s:textfield name="userBean.password" label="Password" />
        <s:submit/>
    </s:form>

</body>
</html>
