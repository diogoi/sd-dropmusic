<%--
  Created by IntelliJ IDEA.
  User: diogo
  Date: 15/12/2018
  Time: 21:57
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
<body>


<jsp:include page="../navbar.jsp"/>

<h1>Create Album</h1>

<s:form action="/create_album">
    <s:textfield name="albumBean.artist_name" label="Artist Name"/>
    <s:textfield name="albumBean.album_name" label="Album Name"/>
    <s:textfield name="albumBean.info" label="Description"/>
    <s:submit/>
</s:form>

</body>
</html>
