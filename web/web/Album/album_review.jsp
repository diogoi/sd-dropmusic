<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>


<jsp:include page="../navbar.jsp"/>
<h1>Search Album</h1>

<s:form action="/create_album_review" >
    <s:textfield name="albumBean.artist_name" label="Artist Name"/>
    <s:textfield name="albumBean.album_name" label="Album Name"/>
    <s:textfield name="albumBean.rate" label="Rate this album (1-10)"/>
    <s:textfield name="albumBean.review" label="Write a review(300 chars)"/>
    <s:submit />
</s:form>

</body>
</html>
