package RMI;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface RMIInterface extends Remote {
    String testConnection() throws RemoteException;
    String signIn(String username, String password) throws RemoteException;
    String signUp(String username, String password) throws RemoteException;
    String createArtist(String name) throws RemoteException;
    String createAlbum(String name, String artist, String info) throws RemoteException;
    String createSong(String name, String artist, String album) throws RemoteException;
    String deleteSong(String song_name, String album_name, String artist_name) throws RemoteException;
    String deleteAlbum(String album_name, String artist_name) throws RemoteException;
    String deleteArtist(String artist_name) throws RemoteException;
    String writeAlbumReview(String artist, String album, String rate, String review, double user_id);
    String editArtist(String name, String newName) throws RemoteException;
    String editAlbum(String name, String newName, String artist, String new_info, double user_id) throws RemoteException;
    String editMusic(String name, String newName, String artist, String album) throws RemoteException;
    String searchArtist(String name ) throws RemoteException;
    String searchAlbum(String name, String artist) throws RemoteException;
    String searchSong(String name, String artist, String album) throws RemoteException;
}
