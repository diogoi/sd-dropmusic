package models;

public class ArtistModel{
    private String artist_name;
    private String new_artist_name;

    public String getArtist_name() {
        return artist_name;
    }

    public void setArtist_name(String artist_name) {
        this.artist_name = artist_name;
    }

    public String getNew_artist_name() {
        return new_artist_name;
    }

    public void setNew_artist_name(String new_artist_name) {
        this.new_artist_name = new_artist_name;
    }

    @Override
    public String toString() {
        return "Artist Name: " + artist_name;
    }
}