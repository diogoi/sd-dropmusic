package models;

import RMI.RMIInterface;
import actions.MulticastCommunication;
import org.json.simple.JSONObject;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;

public class DropMusicBean {


    private int port = 3999;
    private RMIInterface rmiInterface;
    private MulticastCommunication communicate;

    public DropMusicBean() throws RemoteException, NotBoundException, MalformedURLException {
        this.rmiInterface = (RMIInterface) LocateRegistry.getRegistry(this.port).lookup("dropmusic");
        System.out.println(this.rmiInterface.testConnection());
    }


    public String createArtist(String name) throws RemoteException {

        String createArtist;
        createArtist = rmiInterface.createArtist(name);
        System.out.println(createArtist);

        return createArtist;
    }
}
