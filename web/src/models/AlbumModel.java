package models;

public class AlbumModel {

    private String album_name;
    private String artist_name;
    private String info;
    private String rate;
    private String review;
    private String new_album_name;
    private String new_info;


    public String getAlbum_name() {
        return album_name;
    }

    public void setAlbum_name(String album_name) {
        this.album_name = album_name;
    }

    public String getArtist_name() {
        return artist_name;
    }

    public void setArtist_name(String artist_name) {
        this.artist_name = artist_name;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getReview() {
        return review;
    }

    public void setReview(String review) {
        this.review = review;
    }

    public String getNew_album_name() {
        return new_album_name;
    }

    public void setNew_album_name(String new_album_name) {
        this.new_album_name = new_album_name;
    }

    public String getNew_info() {
        return new_info;
    }

    public void setNew_info(String new_info) {
        this.new_info = new_info;
    }

    @Override
    public String toString() {
        return "AlbumModel{" +
                "album_name='" + album_name + '\'' +
                ", artist_name='" + artist_name + '\'' +
                ", info='" + info + '\'' +
                '}';
    }
}
