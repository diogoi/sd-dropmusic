package models;

public class SongModel {

    private String album_name;
    private String artist_name;
    private String song_name;
    private String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getAlbum_name() {
        return album_name;
    }

    public void setAlbum_name(String album_name) {
        this.album_name = album_name;
    }

    public String getArtist_name() {
        return artist_name;
    }

    public void setArtist_name(String artist_name) {
        this.artist_name = artist_name;
    }

    public String getSong_name() {
        return song_name;
    }

    public void setSong_name(String song_name) {
        this.song_name = song_name;
    }

    @Override
    public String toString() {
        return "SongModel{" +
                "album_name='" + album_name + '\'' +
                ", artist_name='" + artist_name + '\'' +
                ", song_name='" + song_name + '\'' +
                ", url='" + url + '\'' +
                '}';
    }
}
