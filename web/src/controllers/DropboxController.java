package controllers;

import com.github.scribejava.core.builder.ServiceBuilder;
import com.github.scribejava.core.model.Token;
import com.github.scribejava.core.model.Verifier;
import com.github.scribejava.core.oauth.OAuthService;
import org.json.simple.JSONObject;
import uc.sd.apis.DropBoxApi2;

import java.util.HashMap;
import java.util.Map;

public class DropboxController extends  ApplicationController {
    private static final String API_APP_KEY = "g0e4905mxwaev0c";
    private static final String API_APP_SECRET = "7rfa0phbpard5bt";
    String url = "";
    String code = "";
    boolean associate_dropbox;
    OAuthService service;

    public String new_oauth() {

        if (this.associate_dropbox) {
            this.service = new ServiceBuilder()
                    .provider(DropBoxApi2.class)
                    .apiKey(API_APP_KEY)
                    .apiSecret(API_APP_SECRET)
                    .callback("http://localhost:8080/web/associate_dropbox")
                    .build();
        } else {
            this.service = new ServiceBuilder()
                    .provider(DropBoxApi2.class)
                    .apiKey(API_APP_KEY)
                    .apiSecret(API_APP_SECRET)
                    .callback("http://localhost:8080/web/login_dropbox")
                    .build();
        }

        url = service.getAuthorizationUrl(null);
        System.out.println(url);

        return "redirect";
    }

    public String oauth_redirect() {
        Verifier verifier = new Verifier(this.code);
        Token accessToken = service.getAccessToken(null, verifier);

        System.out.println("Define API_USER_TOKEN: " + accessToken.getToken());
        System.out.println("Define API_USER_SECRET: " + accessToken.getSecret());

        url = "/home";
        return "redirect";
    }

    public String associate_dropbox() {
        this.service = new ServiceBuilder()
                .provider(DropBoxApi2.class)
                .apiKey(API_APP_KEY)
                .apiSecret(API_APP_SECRET)
                .callback("http://localhost:8080/web/associate_dropbox")
                .build();
        Verifier verifier = new Verifier(this.code);
        Token accessToken = service.getAccessToken(null, verifier);

        System.out.println("token: " + accessToken.getToken());
        System.out.println("secret: " + accessToken.getSecret());
        System.out.println("raw: " + accessToken.getRawResponse());

        Map<String, String> myMap = new HashMap<String, String>();
        String[] pairs = accessToken.getRawResponse().substring(5).split(",");
        for (int i=0;i<pairs.length;i++) {
            String pair = pairs[i];
            String[] keyValue = pair.split(":");
            String trimmed0 = keyValue[0].trim();
            String trimmed1 = keyValue[1].trim();
            myMap.put(String.valueOf(trimmed0.substring(1, trimmed0.length()-1)), String.valueOf(trimmed1.substring(1, trimmed1.length()-1)));
        }

        JSONObject msg = new JSONObject();
        msg.put("type", "associate_dropbox");
        msg.put("user_id", this.session.get("user_id"));
        msg.put("token", accessToken.getToken());
        msg.put("secret", accessToken.getSecret());
        msg.put("acc_id", myMap.get("uid"));
        System.out.println(msg);
        JSONObject response = communicate.exchange(msg);

        url = "/home";
        return "redirect";
    }

    public String login_dropbox() {
        this.service = new ServiceBuilder()
                .provider(DropBoxApi2.class)
                .apiKey(API_APP_KEY)
                .apiSecret(API_APP_SECRET)
                .callback("http://localhost:8080/web/login_dropbox")
                .build();
        Verifier verifier = new Verifier(this.code);
        Token accessToken = service.getAccessToken(null, verifier);

        Map<String, String> myMap = new HashMap<String, String>();
        String[] pairs = accessToken.getRawResponse().substring(5).split(",");
        for (int i=0;i<pairs.length;i++) {
            String pair = pairs[i];
            String[] keyValue = pair.split(":");
            String trimmed0 = keyValue[0].trim();
            String trimmed1 = keyValue[1].trim();
            myMap.put(String.valueOf(trimmed0.substring(1, trimmed0.length()-1)), String.valueOf(trimmed1.substring(1, trimmed1.length()-1)));
        }

        JSONObject msg = new JSONObject();
        msg.put("type", "login_dropbox");
        msg.put("token", accessToken.getToken());
        msg.put("acc_id", myMap.get("uid"));
        JSONObject response = communicate.exchange(msg);

        if((boolean) response.get("success")) {
            this.session.put("user_id", response.get("user_id"));
            url = "/home";
        } else {
            url = "/new_login";
        }
        return "redirect";
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public boolean isAssociate_dropbox() {
        return associate_dropbox;
    }

    public void setAssociate_dropbox(boolean associate_dropbox) {
        this.associate_dropbox = associate_dropbox;
    }
}
