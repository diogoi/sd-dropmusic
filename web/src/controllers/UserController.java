package controllers;

import models.UserModel;
import org.json.simple.JSONObject;

public class UserController extends ApplicationController {
    private static int port = 3333;
    private UserModel userBean;
    private String url;
    private MulticastCommunication communicate = new MulticastCommunication();

    private String test;

    public String new_register() {
        return SUCCESS;
    }

    public String new_login() {
        return SUCCESS;
    }

    public String create_register() {
        JSONObject msg = new JSONObject();
        System.out.println(userBean);
        msg.put("type", "register");
        msg.put("username", userBean.getUsername());
        msg.put("password", userBean.getPassword());
        JSONObject response = communicate.exchange(msg);

        url = "/new_login";
        return "redirect";
    }

    public String create_login() {
        JSONObject msg = new JSONObject();
        msg.put("type", "login");
        msg.put("username", userBean.getUsername());
        msg.put("password", userBean.getPassword());
        JSONObject response = communicate.exchange(msg);

        if((boolean) response.get("success")) {
            url = "/home";
            this.session.put("user_id", response.get("user_id"));
            return "redirect_home";
        } else {
            url = "/new_login";
            return "redirect_login";
        }
    }

    public String new_give_powers() {
        return SUCCESS;
    }

    public String create_give_powers() {
        JSONObject msg = new JSONObject();
        msg.put("username", userBean.getUsername());
        msg.put("type", "give_powers");
        JSONObject response = communicate.exchange(msg);
        System.out.println(response);

        url = "/home";
        return "redirect";
    }

    public String logout(){
        this.session.remove("user_id");
        url = "/home";
        return "redirect";
    }

    public UserModel getUserBean() {
        return userBean;
    }

    public void setUserBean(UserModel userBean) {
        this.userBean = userBean;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

    public String getTest() {
        return test;
    }

    public void setTest(String test) {
        this.test = test;
    }
}