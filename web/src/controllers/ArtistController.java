package controllers;

import actions.MulticastCommunication;
import com.opensymphony.xwork2.ActionSupport;
import models.ArtistModel;
import models.DropMusicBean;
import org.apache.struts2.interceptor.SessionAware;
import org.json.simple.JSONObject;

import java.util.Map;

public class ArtistController extends ApplicationController {
    private static int port = 3333;
    private ArtistModel artistBean;
    private String url;
    private controllers.MulticastCommunication communicate = new controllers.MulticastCommunication();
    private String show_info;

    public String new_create_artist() { return SUCCESS; }

    public String new_edit_artist() { return SUCCESS; }

    public String new_delete_artist() { return SUCCESS; }

    public String new_search_artist () { return SUCCESS; }

    public String create_artist(){
        JSONObject msg = new JSONObject();
        msg.put("type", "create_artist");
        msg.put("name", artistBean.getArtist_name());
        JSONObject response = communicate.exchange(msg);

        url = "/home";
        return "redirect";
    }

    public String edit_artist(){
        JSONObject msg = new JSONObject();
        msg.put("type", "edit_artist");
        msg.put("artist_name", artistBean.getArtist_name());
        msg.put("new_artist_name", artistBean.getNew_artist_name());
        JSONObject response = communicate.exchange(msg);

        url = "/home";
        return "redirect";
    }

    public String delete_artist() {
        JSONObject msg = new JSONObject();
        msg.put("type", "delete_artist");
        msg.put("artist_name", artistBean.getArtist_name());
        JSONObject response = communicate.exchange(msg);

        url = "/home";
        return "redirect";
    }

    public String search_artist() {
        return SUCCESS;
    }

    public String show_artist() {
        JSONObject msg = new JSONObject();
        msg.put("type", "search_artist");
        msg.put("artist_name", artistBean.getArtist_name());
        JSONObject response = communicate.exchange(msg);
        System.out.println(response);

        if((boolean) response.get("success")) {
            System.out.println(response);
            this.show_info = (String)response.get("message");
            return SUCCESS;
        } else {
            System.out.println("AAAAAAAA");
            url = "/web/search_artist";
            return "fail";
        }
    }


    public void setUrl(String url) {
        this.url = url;
    }

    public String getUrl(){
        return url;
    }

    public ArtistModel getArtistBean() {
        return artistBean;
    }

    public void setArtistBean(ArtistModel artistBean) {
        this.artistBean = artistBean;
    }

    public String getShow_info() {
        return show_info;
    }

    public void setShow_info(String show_info) {
        this.show_info = show_info;
    }
}
