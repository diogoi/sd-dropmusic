package controllers;

import com.opensymphony.xwork2.ActionSupport;
import models.AlbumModel;
import org.apache.struts2.interceptor.SessionAware;
import org.json.simple.JSONObject;

import java.util.Map;

public class AlbumController extends ApplicationController{


    private static int port = 3333;
    private AlbumModel albumBean;
    private String url;
    private MulticastCommunication communicate = new MulticastCommunication();
    private String show_info;


    public String new_create_album(){
        return SUCCESS;
    }

    public String new_create_album_review() { return SUCCESS; }

    public String new_edit_album() { return SUCCESS; }

    public String search_album() { return SUCCESS; }

    public String create_album() {
        JSONObject msg = new JSONObject();
        double userId = Double.parseDouble(this.session.get("user_id").toString());
        msg.put("type", "create_album");
        msg.put("name", albumBean.getAlbum_name());
        msg.put("artist", albumBean.getArtist_name());
        msg.put("info", albumBean.getInfo());
        msg.put("user_id", userId);
        JSONObject response = communicate.exchange(msg);

        url = "/home";
        return "redirect";
    }

    public String create_album_review(){
        JSONObject msg = new JSONObject();
        double userId = Double.parseDouble(this.session.get("user_id").toString());
        System.out.println(userId);
        msg.put("type", "write_album_review");
        msg.put("album_name", albumBean.getAlbum_name());
        msg.put("artist_name", albumBean.getArtist_name());
        msg.put("rate", albumBean.getRate());
        msg.put("review", albumBean.getReview());
        msg.put("user_id", userId);
        JSONObject response = communicate.exchange(msg);

        url = "/home";
        return "redirect";
    }
    public String edit_album(){
        JSONObject msg = new JSONObject();
        double userId = Double.parseDouble(this.session.get("user_id").toString());
        msg.put("type", "edit_album");
        msg.put("album_name", albumBean.getAlbum_name());
        msg.put("new_album_name", albumBean.getNew_album_name());
        msg.put("artist_name", albumBean.getArtist_name());
        msg.put("info", albumBean.getNew_info());
        msg.put("user_id", userId);
        JSONObject response = communicate.exchange(msg);

        url = "/";
        return "redirect";
    }

    public String show_album() {
        JSONObject msg = new JSONObject();
        msg.put("type", "search_album");
        msg.put("album_name", albumBean.getAlbum_name());
        msg.put("artist_name", albumBean.getArtist_name());
        JSONObject response = communicate.exchange(msg);
        System.out.println(response);
        if((boolean) response.get("success")) {
            System.out.println(response);
            this.show_info = (String)response.get("message");
            return SUCCESS;
        } else {
            url = "/web/search_album";
            return "fail";
        }
    }

    public String search() { return SUCCESS; }

    public String delete() { return SUCCESS; }

    public String album_review() { return SUCCESS; }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

    public AlbumModel getAlbumBean() {
        return albumBean;
    }

    public void setAlbumBean(AlbumModel albumBean) {
        this.albumBean = albumBean;
    }

    public String getShow_info() {
        return show_info;
    }

    public void setShow_info(String show_info) {
        this.show_info = show_info;
    }
}
