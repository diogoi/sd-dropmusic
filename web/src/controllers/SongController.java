package controllers;


import com.opensymphony.xwork2.ActionSupport;
import models.SongModel;
import org.apache.struts2.interceptor.SessionAware;
import org.json.simple.JSONObject;

import java.util.Map;

public class SongController extends ApplicationController{

    private static int port = 3333;
    private SongModel songBean;
    private String url;
    private controllers.MulticastCommunication communicate = new controllers.MulticastCommunication();


    public String edit(){
        return SUCCESS;
    }

    public String new_create_song (){
        return SUCCESS;
    }

    public String delete() { return SUCCESS; }

    @Override
    public void setSession(Map<String, Object> map) {

    }

    public String create_song() {
        JSONObject msg = new JSONObject();
        msg.put("type", "create_music");
        msg.put("name", songBean.getSong_name());
        msg.put("artist", songBean.getArtist_name());
        msg.put("album", songBean.getAlbum_name());
        msg.put("url", songBean.getUrl());
        System.out.println("oi " + msg.toString());
        JSONObject response = communicate.exchange(msg);

        url = "/home";
        return "redirect";
    }

    public String search_song() {
        return SUCCESS;
    }

    public String show_song() {
        JSONObject msg = new JSONObject();
        msg.put("type", "search_music");
        msg.put("music_name", songBean.getSong_name());
        msg.put("album_name", songBean.getAlbum_name());
        msg.put("artist_name", songBean.getArtist_name());
        JSONObject response = communicate.exchange(msg);
        this.songBean.setUrl("https://dl.dropboxusercontent.com" + ((String) response.get("url")).substring(23));
        System.out.println(this.songBean);

        return SUCCESS;
    }

    public SongModel getSongBean() {
        return songBean;
    }

    public void setSongBean(SongModel songBean) {
        this.songBean = songBean;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}