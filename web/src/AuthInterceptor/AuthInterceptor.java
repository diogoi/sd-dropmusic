
package AuthInterceptor;

import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.Interceptor;


import java.util.Map;

public class AuthInterceptor implements Interceptor {
    private static final long serialVersionUID = 189237412378L;

    @Override
    public String intercept(ActionInvocation invocation) throws Exception {
        Map<String, Object> session = invocation.getInvocationContext().getSession();
        System.out.println("session: " + session);

        if(session.get("user_id") != null) {
            return invocation.invoke();
        } else {
            return Action.ERROR;
        }
    }

    @Override
    public void init() { }

    @Override
    public void destroy() { }
}