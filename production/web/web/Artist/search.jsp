<%--
  Created by IntelliJ IDEA.
  User: diogo
  Date: 16/12/2018
  Time: 16:02
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<jsp:include page="../navbar.jsp"/>
<h1>Search Album</h1>

<s:form action="/show_artist" method="post">
    <s:textfield name="artistBean.artist_name" label="Artist Name"/>
    <s:submit type="submit"/>
</s:form>

</body>
</html>
