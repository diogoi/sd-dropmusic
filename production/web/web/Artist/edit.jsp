<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<jsp:include page="../navbar.jsp"/>
<h1>Edit Artist</h1>
<s:form action="/edit_artist" method="post">
    <s:textfield name="artistBean.artist_name" label="Artist Name"/>
    <s:textfield name="artistBean.new_artist_name" label="New Artist Name" />
    <s:submit type="submit"/>
</s:form>

</body>
</html>
