<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>

    <title>Title</title>
</head>
<body>
<jsp:include page="../navbar.jsp"/>
<h1>Create Artist</h1>
    <s:form action="/create_artist" >
        <s:textfield name="artistBean.artist_name" label="Artist Name"/>
        <s:submit />
    </s:form>
</body>
</html>
