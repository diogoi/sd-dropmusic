<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <title>$Title$</title>
</head>
<body>
<div class="container">
    <h1>Menu</h1>
    <nav class="navbar navbar-dark bg-dark">
        <button onclick="window.location.href='/web/logout'">Logout</button>
    </nav>
    <ul class="list-group">
        <li class="list-group-item">
            <a href="/web/search_artist">Search Artist</a>
        </li>
        <li class="list-group-item">
            <a href="web/new_create_artist">Create Artist</a>
        </li>
        <li class="list-group-item">
            <a href="/new_edit_artist">Edit Artist</a>
        </li>
        <li class="list-group-item">
            <a href="/new_delete_artist">Delete Artist</a>
        </li>
        <li class="list-group-item">
            <a href="web/search_album">Search Album</a>
        </li>
        <li class="list-group-item">
            <a href="/web/new_create_album">Create Album</a>
        </li>
        <li class="list-group-item">
            <a href="/new_edit_album">Edit Album</a>
        </li>
        <li class="list-group-item">
            <a href="/web/search_song">Search Song</a>
        </li>
        <li class="list-group-item">
            <a href="/web/new_create_song">Create Song</a>
        </li>
        <li class="list-group-item">
            <a href="/new_edit_song">Edit Song</a>
        </li>
        <li class="list-group-item">
            <a href="/create_album_review">Create Album Review</a>
        </li>
        <li class="list-group-item">
            <a href="/web/new_give_powers">Give Powers</a>
        </li>
        <li class="list-group-item">
            <a href="/web/dropbox?associate_dropbox=true">Associate Dropbox</a>
        </li>
    </ul>
</div>
</body>
</html>
