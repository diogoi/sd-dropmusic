<%--
  Created by IntelliJ IDEA.
  User: diogo
  Date: 15/12/2018
  Time: 21:57
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<jsp:include page="../navbar.jsp"/>
<h1>Edit Album</h1>
<s:form action="/edit_album">
    <s:textfield name="albumBean.artist_name" label="Artist Name" />
    <s:textfield name="albumBean.album_name" label="Album Name" />
    <s:textfield name="albumBean.new_album_name" label="New Album Name" />
    <s:textfield name="albumBean.new_info" label="Updated Info" />
    <s:submit />
</s:form>

</body>
</html>
