<%--
  Created by IntelliJ IDEA.
  User: diogo
  Date: 16/12/2018
  Time: 12:40
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<jsp:include page="../navbar.jsp"/>
<h1>Search Album</h1>

<s:form action="/show_album" >
    <s:textfield name="albumBean.artist_name" label="Artist Name"/>
    <s:textfield name="albumBean.album_name" label="Album Name"/>
    <s:submit type="submit"/>
</s:form>
</body>
</html>
