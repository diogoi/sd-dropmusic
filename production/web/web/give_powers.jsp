<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Give Powers</title>
</head>
<body>

<h3>Promote User to Editor</h3>

<s:form action="/create_give_powers">
    <s:textfield name="userBean.username" label="Username" />
    <s:submit/>
</s:form>

</body>
</html>