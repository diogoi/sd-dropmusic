import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.io.File;
import java.io.IOException;
import java.net.*;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.util.Scanner;

public class Client {
    private int port = 3333;
    private RMIServerInterface rmiInterface;
    private double user_id = -1;
    private static String multiCastAddress = "224.0.224.0";
    private static int multiCastPort = 3335;
    private Thread thread;
    private boolean editor = false;

    public Client() throws RemoteException, NotBoundException {
        this.rmiInterface = (RMIServerInterface) LocateRegistry.getRegistry(this.port).lookup("dropmusic");
        System.out.println("RMIClient ready");
        System.out.println(this.rmiInterface.testConnection());
    }

    public void setEditor(boolean editor) {
        this.editor = editor;
    }


    /**
     * The menu displayed checks if the user is logged in or not
     */
    public void menu() throws RemoteException {
        while (true) {
            if (isLoggedIn() && this.editor) {
                menuLoggedInEditor();
            } else if (isLoggedIn() && !this.editor) {
                menuLoggedInNonEditor();
            } else {
                menuNotLoggedIn();
            }
        }
    }
    private void menuLoggedInNonEditor() throws RemoteException {
        System.out.println("1 - Search\n" +
                "2 - Album review\n" +
                "3 - Download music\n" +
                "4 - Upload music\n" +
                "10 - Logout\n"
        );
        Scanner sc = new Scanner(System.in);
        int option = sc.nextInt();
        switch (option) {
            case 1:
                menuSearch();
                break;

            case 2:
                writeAlbumReview();
                break;

            case 3:
                downloadMusic();
                break;

            case 4:
                uploadMusic();
                break;

            case 10:
                try {
                    Logout();
                } catch (InterruptedException | IOException e) {
                    e.printStackTrace();
                }
                break;
            case 0:
                System.exit(0);
        }
    }




    private void menuLoggedInEditor() throws RemoteException {
        System.out.println("1 - Search\n" +
                "2 - Create\n" +
                "3 - Edit\n" +
                "4 - Delete\n" +
                "5 - Album review\n" +
                "6 - Give powers\n" +
                "7 - Download music\n" +
                "8 - Upload music\n" +
                "9 - Logout\n"
        );
        Scanner sc = new Scanner(System.in);
        int option = sc.nextInt();
        switch (option) {
            case 1:
                menuSearch();
                break;
            case 2:
                menuCreate();
                break;

            case 3:
                menuEdit();
                break;

            case 4:
                menuDelete();
                break;

            case 5:
                writeAlbumReview();
                break;

            case 6:
                givePowers();
                break;

            case 7:
                downloadMusic();
                break;

            case 8:
                uploadMusic();
                break;

            case 9:
                try {
                    Logout();
                } catch (InterruptedException | IOException e) {
                    e.printStackTrace();
                }
                break;
            case 0:
                System.exit(0);
        }
    }

    private void givePowers() throws RemoteException {
        Scanner sc = new Scanner(System.in);
        System.out.println("Username: ");
        String username = sc.nextLine();
        System.out.println(this.rmiInterface.givePowers(username));
    }


    private void downloadMusic() throws RemoteException {
        Scanner sc = new Scanner(System.in);
        System.out.println("Artist name: ");
        String artist_name = sc.nextLine();
        System.out.println("Album name: ");
        String album_name = sc.nextLine();
        System.out.println("Music name: ");
        String music_name = sc.nextLine();
        JSONObject response = this.rmiInterface.downloadMusic(artist_name, album_name, music_name);
        System.out.println(response.get("message"));
        System.out.println(response);
        if ((boolean) response.get("success")) {
            int port = Integer.parseInt((String) response.get("port"));
            FileTransferDownload.receive((String) response.get("address"), port, (String) response.get("filename"));

        }
    }

    private void menuSearch() throws RemoteException {
        System.out.println("1 - Artist\n" +
                "2 - Album\n" +
                "3 - Music\n" +
                "4 - Global search"
        );
        Scanner sc = new Scanner(System.in);
        int option = sc.nextInt();
        switch (option) {
            case 1:
                artistSearch();
                break;
            case 2:
                searchAlbum();
                break;
            case 3:
                musicSearch();
                break;
            case 4:
                globalSearch();
                break;
        }

    }

    private void uploadMusic() throws RemoteException {
        Scanner sc = new Scanner(System.in);
        System.out.println("Artist name: ");
        String artist_name = sc.nextLine();
        System.out.println("Album name: ");
        String album_name = sc.nextLine();
        System.out.println("Music name: ");
        String music_name = sc.nextLine();
        System.out.println("Filepath: ");
        String filepath = sc.nextLine();
        FileTransferUpload FT = new FileTransferUpload(filepath);
        Thread thread= new Thread(FT);
        thread.start();
        JSONObject response = this.rmiInterface.uploadMusic(artist_name, album_name, music_name, filepath, FT.getPort());
        System.out.println(response.get("message"));
        System.out.println(response);
        if(!(boolean) response.get("success")) {
            thread.currentThread().interrupt();
        }
        System.out.println((String) response.get("message"));
    }



    private void globalSearch() throws RemoteException {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter your search term: ");
        String term = sc.nextLine();
        JSONObject results = this.rmiInterface.globalSearch(term);

        String artistResults = (String) results.get("artist");
        String albumResults = (String) results.get("album");

        System.out.println("Artists : \n");
        System.out.println(artistResults);
        System.out.println("Album: \n");
        System.out.println(albumResults);

        System.out.println("More details: \n" +
                "1 - Artist\n" +
                "2 - Album\n" +
                "0 - Leave");

        int option = sc.nextInt();
        switch (option){
            case 1:
                System.out.println("Artist ID:");
                String artistID = sc.next();
                System.out.println(this.rmiInterface.getArtistById(artistID));
                break;
            case 2:
                System.out.println("Album ID:");
                String albumID = sc.next();
                System.out.println(this.rmiInterface.getAlbumById(albumID));
                break;
            case 0:
                break;
        }

    }

    private void menuNotLoggedIn() throws RemoteException {
        int option = loginOptions();
        switch (option) {
            case 1:
                signUp();
                break;
            case 2:
                signIn();
                break;
            case 0:
                System.exit(0);
        }
    }

    private void menuEdit() throws RemoteException {
        System.out.println("1 - Artist\n" +
                "2 - Album\n" +
                "3 - Music\n"
        );
        Scanner sc = new Scanner(System.in);
        int option = sc.nextInt();
        switch (option) {
            case 1:
                editArtist();
                break;
            case 2:
                editAlbum();
                break;
            case 3:
                editMusic();
                break;
        }
    }

    private void editArtist() throws RemoteException {
        Scanner sc = new Scanner(System.in);
        System.out.println("Artist Name: ");
        String name = sc.nextLine();
        System.out.println("New artist name: ");
        String newName = sc.nextLine();
        System.out.println(this.rmiInterface.editArtist(name, newName));
    }

    private void editAlbum() throws RemoteException {
        Scanner sc = new Scanner(System.in);
        System.out.println("Artist Name: ");
        String artist = sc.nextLine();
        System.out.println("Album Name: ");
        String name = sc.nextLine();
        System.out.println("New album name: ");
        String newName = sc.nextLine();
        System.out.println("New album info: ");
        String new_info = sc.nextLine();
        System.out.println(this.rmiInterface.editAlbum(name, newName, artist, new_info, this.user_id));
    }

    private void editMusic() throws RemoteException {
        Scanner sc = new Scanner(System.in);
        System.out.println("Album Name: ");
        String album = sc.nextLine();
        System.out.println("Artist Name: ");
        String artist = sc.nextLine();
        System.out.println("Song Name: ");
        String name = sc.nextLine();
        System.out.println("New song name: ");
        String newName = sc.nextLine();
        System.out.println(this.rmiInterface.editMusic(name, newName, artist, album));
    }

    private boolean isLoggedIn() {
        if (this.user_id != -1) {
            return true;
        }
        return false;
    }

    private void menuCreate() throws RemoteException {
        System.out.println("1 - Artist\n" +
                "2 - Album\n" +
                "3 - Music\n"
        );
        Scanner sc = new Scanner(System.in);
        int option = sc.nextInt();
        switch (option) {
            case 1:
                createArtist();
                break;
            case 2:
                createAlbum();
                break;
            case 3:
                createMusic();
                break;
        }
    }

    private void menuDelete() throws RemoteException {
        System.out.println("1 - Artist\n" +
                "2 - Album\n" +
                "3 - Music\n"
        );
        Scanner sc = new Scanner(System.in);
        int option = sc.nextInt();
        switch (option) {
            case 1:
                deleteArtist();
                break;
            case 2:
                deleteAlbum();
                break;
            case 3:
                deleteMusic();
                break;
        }
    }

    private void musicSearch() throws RemoteException {
        Scanner sc = new Scanner(System.in);
        System.out.println("Music to Search : ");
        String musicInput = sc.nextLine();
        System.out.println("Artist: ");
        String artistInput = sc.nextLine();
        System.out.println("Album: ");
        String albumInput = sc.nextLine();
        System.out.println(this.rmiInterface.searchMusic(musicInput, artistInput, albumInput));

    }

    private void searchAlbum() throws RemoteException {
        Scanner sc = new Scanner(System.in);
        System.out.println("Album to Search : ");
        String albumInput = sc.nextLine();
        System.out.println("Artist: ");
        String artistInput = sc.nextLine();
        System.out.println(this.rmiInterface.searchAlbum(albumInput, artistInput));
    }

    private void artistSearch() throws RemoteException {
        System.out.println("Artist to Search : ");
        Scanner sc = new Scanner(System.in);
        String artistInput = sc.nextLine();
        System.out.println(this.rmiInterface.searchArtist(artistInput));
    }

    private void writeAlbumReview() throws RemoteException {
        Scanner sc = new Scanner(System.in);
        System.out.println("Artist");
        String artist = sc.nextLine();
        System.out.println("Album");
        String album = sc.nextLine();
        System.out.println("Rate this album (1-10): ");
        String rate = sc.nextLine();
        System.out.println("Write a review(max 300 chars): ");
        String review = sc.nextLine();
        System.out.println(this.rmiInterface.writeAlbumReview(artist, album, rate, review, this.user_id));
    }

    private void deleteArtist() throws RemoteException {
        Scanner sc = new Scanner(System.in);
        System.out.println("Artist name: ");
        String artist_name = sc.nextLine();
        System.out.println(this.rmiInterface.deleteArtist(artist_name));
    }

    private void deleteAlbum() throws RemoteException {
        Scanner sc = new Scanner(System.in);
        System.out.println("Artist name: ");
        String artist_name = sc.nextLine();
        System.out.println("Album name: ");
        String album_name = sc.nextLine();
        System.out.println(this.rmiInterface.deleteAlbum(album_name, artist_name));
    }

    private void deleteMusic() throws RemoteException {
        Scanner sc = new Scanner(System.in);
        System.out.println("Artist name: ");
        String artist_name = sc.nextLine();
        System.out.println("Album name: ");
        String album_name = sc.nextLine();
        System.out.println("Music name: ");
        String music_name = sc.nextLine();
        System.out.println(this.rmiInterface.deleteMusic(music_name, album_name, artist_name));
    }

    private void createArtist() throws RemoteException {
        Scanner sc = new Scanner(System.in);
        System.out.println("Name: ");
        String name = sc.nextLine();
        System.out.println(this.rmiInterface.createArtist(name));
    }

    private void createAlbum() throws RemoteException {
        Scanner sc = new Scanner(System.in);
        System.out.println("Name: ");
        String name = sc.nextLine();
        System.out.println("Artist: ");
        String artist = sc.nextLine();
        System.out.println("info: ");
        String info = sc.nextLine();
        System.out.println(this.rmiInterface.createAlbum(name, artist, info, this.user_id));
    }

    private void createMusic() throws RemoteException {
        Scanner sc = new Scanner(System.in);
        System.out.println("Name: ");
        String name = sc.nextLine();
        System.out.println("Artist: ");
        String artist = sc.nextLine();
        System.out.println("Album: ");
        String album = sc.nextLine();
        System.out.println(this.rmiInterface.createMusic(name, artist, album));
    }

    private void signUp() throws RemoteException {
        Scanner sc = new Scanner(System.in);
        System.out.println("Username: ");
        String username = sc.nextLine();
        System.out.println("Password: ");
        String password = sc.nextLine();
        System.out.println(this.rmiInterface.signUp(username, password));
    }

    private void signIn() throws RemoteException {
        Scanner sc = new Scanner(System.in);
        System.out.println("Username: ");
        String username = sc.nextLine();
        System.out.println("Password: ");
        String password = sc.nextLine();
        JSONObject response = this.rmiInterface.signIn(username, password);
        if ((boolean) response.get("success")) {
            this.editor = (boolean) response.get("editor");
            System.out.println(this.rmiInterface.checkMessages((double) response.get("user_id")));
            this.user_id = (double) response.get("user_id");
            Notification notifications = new Notification(this.user_id, this);
            this.thread = new Thread(notifications);
            this.thread.start();
        }
    }

    private static int loginOptions() {
        System.out.println("Choose a number\n" +
                "1 - Register\n" +
                "2 - Login\n" +
                "0 - Leave\n"
        );
        Scanner sc = new Scanner(System.in);
        int option = sc.nextInt();
        return option;
    }

    private void Logout() throws InterruptedException, IOException {
        JSONObject msg = new JSONObject();
        JSONArray user_ids = new JSONArray();
        user_ids.add(Double.toString(this.user_id));
        msg.put("status", "down");
        msg.put("user_ids", user_ids);
        byte[] buffer = msg.toString().getBytes();
        MulticastSocket socket = new MulticastSocket(this.multiCastPort);
        InetAddress group = InetAddress.getByName(this.multiCastAddress);
        DatagramPacket packet = new DatagramPacket(buffer, buffer.length, group, this.multiCastPort);
        socket.send(packet);
        this.thread.join();
        this.rmiInterface.logout(this.user_id);
        this.user_id = -1;
    }
}