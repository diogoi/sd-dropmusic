import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.util.Iterator;

public class Notification implements Runnable{
    private String multicastAddress = "224.0.224.0";
    private int multiCastReceivePort = 3335;
    private MulticastSocket socket = null;
    private double user_id;
    private Client user;

    public Notification(double user_id, Client user) {
        this.user = user;
        this.user_id = user_id;
    }

    public void run() {
        try {
            joinGroup();
            JSONParser parser = new JSONParser();

            //noinspection InfiniteLoopStatement
            while (true) {
                JSONObject message = (JSONObject) parser.parse(getMessage());
                JSONArray user_ids = (JSONArray) message.get("user_ids");
                Iterator<String> iterator = user_ids.iterator();
                while (iterator.hasNext()) {
                    if(Double.parseDouble(iterator.next()) == this.user_id) {
                        if(((String) message.get("status")).equals("down")){
                            this.user_id = -1;
                            this.socket.close();
                            throw new IOException();
                        }
                        System.out.println(message.get("message"));
                        if(((String) message.get("type")).equals("give_powers")){
                            this.user.setEditor(true);
                        }
                    }
                }
            }
        } catch (IOException | ParseException e) {
        } finally {
            socket.close();
        }
    }

    private void joinGroup() throws IOException {
        this.socket = new MulticastSocket(this.multiCastReceivePort);
        InetAddress group = InetAddress.getByName(this.multicastAddress);
        socket.joinGroup(group);
    }

    private String getMessage() throws IOException {
        byte[] buffer = new byte[1024];
        DatagramPacket packet = new DatagramPacket(buffer, buffer.length);
        socket.receive(packet);
        return new String(packet.getData(), 0, packet.getLength());
    }
}
