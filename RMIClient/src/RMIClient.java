import java.rmi.NotBoundException;
import java.rmi.RemoteException;

public class RMIClient {
    public static void main(String[] args) {
        try {
            Client client = new Client();
            client.menu();
        } catch (RemoteException | NotBoundException e) {
            e.printStackTrace();
        }
    }
}
