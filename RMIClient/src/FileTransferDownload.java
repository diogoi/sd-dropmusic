import java.io.*;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.concurrent.TimeUnit;

public class FileTransferDownload {

    public static void receive(String address, int port, String filename) {
        //Initialize socket
        Socket socket = null;
        byte[] contents = new byte[10000];

        //Initialize the FileOutputStream to the output file's full path.
        FileOutputStream fos = null;
        InputStream is = null;

        try {
            socket = new Socket(InetAddress.getByName(address), port);
            fos = new FileOutputStream(filename);
            is = socket.getInputStream();
        } catch (IOException e) {
            e.printStackTrace();
        }

        BufferedOutputStream bos = new BufferedOutputStream(fos);

        //No of bytes read in one read() call
        int bytesRead = 0;

        try {
            while ((bytesRead = is.read(contents)) != -1)
                bos.write(contents, 0, bytesRead);

            is.close();
            bos.flush();
            bos.close();
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("File saved successfully!");
    }
}

